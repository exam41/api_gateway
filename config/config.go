package config
import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"os"
)
type Config struct {
	ServiceName string
	Environment string
	HTTPPort string
	RepoGRPCServiceHost string
	RepoGRPCServicePort string
	PayGRPCServiceHost string
	PayGRPCServicePort string
}
func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("error!!!", err)
	}
	cfg := Config{}
	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "api_gateway"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))
	cfg.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))
	cfg.PayGRPCServiceHost = cast.ToString(getOrReturnDefault("REPO_SERVICE_GRPC_HOST", "localhost"))
	cfg.PayGRPCServicePort = cast.ToString(getOrReturnDefault("REPO_SERVICE_GRPC_PORT", ":8089"))
	cfg.RepoGRPCServiceHost = cast.ToString(getOrReturnDefault("PAY_GRPC_SERVICE_HOST", "localhost"))
	cfg.RepoGRPCServicePort = cast.ToString(getOrReturnDefault("PAY_GRPC_SERVICE_PORT", ":8098"))
	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}
	return defaultValue
}

