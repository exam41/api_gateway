package api

import (
	_ "api_gateway/api/docs"
	"api_gateway/api/handler"

	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

	r.Use(cors.Middleware(cors.Config{
		Origins:        "*",
		RequestHeaders: "Authorization, Origin, Content-Type",
		Methods:        "POST, GET, PUT, DELETE, OPTION",
	}))

	v1Route := r.Group("/v1")


	v1Route.POST("/branch", h.CreateBranch)
	v1Route.GET("/branch/:id", h.GetBranch)
	v1Route.GET("/branch", h.GetListBranch)
	v1Route.PUT("/branch/:id", h.UpdateBranch)
	v1Route.DELETE("/branch/:id", h.DeleteBranch)

	v1Route.POST("/storage", h.CreateStorage)
	v1Route.GET("/storage/:id", h.GetStorage)
	v1Route.GET("/storage", h.GetListStorage)
	v1Route.PUT("/storage/:id", h.UpdateStorage)
	v1Route.DELETE("/storage/:id", h.DeleteStorage)

	v1Route.POST("/category",h.CreateCategory)
	v1Route.GET("/category/:id",h.GetCategory)
	v1Route.GET("/category",h.GetListCategory)
	v1Route.PUT("/category/:id",h.UpdateCategory)
	v1Route.DELETE("/category/:id",h.DeleteCategory)

	v1Route.POST("/courier",h.CreateCourier)
	v1Route.GET("/courier/:id",h.GetCourier)
	v1Route.GET("/courier",h.GetListCourier)
	v1Route.PUT("/courier/:id",h.UpdateCourier)
	v1Route.DELETE("/courier/:id",h.DeleteCourier)


	v1Route.POST("/incomeproduct",h.CreateIncomeProduct)
	v1Route.GET("/incomeproduct/:id",h.GetIncomeProduct)
	v1Route.GET("/incomeproduct",h.GetListIncomeProduct)
	v1Route.PUT("/incomeproduct/:id",h.UpdateIncomeProduct)
	v1Route.DELETE("/incomeproduct/:id",h.DeleteIncomeProduct)


	v1Route.POST("/income",h.CreateIncome)
	v1Route.GET("/income/:id",h.GetIncome)
	v1Route.GET("/income",h.GetListIncome)
	v1Route.PUT("/income/:id",h.UpdateIncome)
	v1Route.DELETE("/income/:id",h.DeleteIncome)
	v1Route.POST("/incomelogic",h.Incomelogic)



	v1Route.POST("product",h.CreateProduct)
	v1Route.GET("/product/:id",h.GetProduct)
	v1Route.GET("/product",h.GetListProduct)
	v1Route.PUT("/product/:id",h.UpdateProduct)
	v1Route.DELETE("/product/:id",h.DeleteProduct)


	v1Route.POST("/salepoint",h.CreateSalePoint)
	v1Route.GET("/salepoint/:id",h.GetSalePoint)
	v1Route.GET("/salepoint",h.GetListSalePoint)
	v1Route.PUT("/salepoint/:id",h.UpdateSalePoint)
	v1Route.DELETE("/salepoint/:id",h.DeleteSalePoint)


	

	v1Route.POST("/endsale",h.EndSale)

	v1Route.PATCH("/shiftlogic",h.ChangeShiftStatus)



	v1Route.POST("/saleproduct",h.CreateSaleProduct)
	v1Route.GET("/saleproduct/:id",h.GetSaleProduct)
	v1Route.GET("/saleproduct",h.GetListSaleProduct)
	v1Route.PUT("/saleproduct/:id",h.UpdateSaleProduct)
	v1Route.DELETE("/saleproduct/:id",h.DeleteSaleProduct)


	v1Route.POST("/sale",h.CreateSale)
	v1Route.GET("/sale/:id",h.GetSale)
	v1Route.GET("/sale",h.GetListSale)
	v1Route.PUT("/sale/:id",h.UpdateSale)
	v1Route.DELETE("/sale/:id",h.DeleteSale)


	v1Route.POST("/shift",h.CreateShift)
	v1Route.GET("/shift/:id",h.GetShift)
	v1Route.GET("/shift",h.GetListShift)
	v1Route.PUT("/shift/:id",h.UpdateShift)
	v1Route.DELETE("/shift/:id",h.DeleteShift)
	v1Route.PATCH("shift/:id",h.UpdateStatusShift)


	v1Route.POST("/staff",h.CreateStaff)
	v1Route.GET("/staff/:id",h.GetStaff)
	v1Route.GET("/staff",h.GetListStaff)
	v1Route.PUT("/staff/:id",h.UpdateStaff)
	v1Route.DELETE("/staff/:id",h.DeleteStaff)




	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
