package models

type Branch struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Phone   string `json:"phone"`
}
type BranchResponse struct {
	Branchs []Branch `json:"branchs"`
	Count   int      `json:"count"`
}