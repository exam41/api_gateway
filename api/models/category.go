package models

type Category struct{
	ID   string `json:"id"`
	Name string `json:"name"`
	ParentID string `json:"parent_id"`
}
type CreateCategory struct{
	Name string `json:"name"`
}
type CategoryResponse struct{
	Categorys []Category `json:"categories"`
	Count int `json:"count"`
}
