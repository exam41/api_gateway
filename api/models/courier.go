package models

type Courier struct{
	ID   string `json:"id"`
	Name string `json:"name"`
	Phone string `json:"phone"`
	Active bool `json:"active"`
}
type CreateCourier struct{
	Name string `json:"name"`
	Phone string `json:"phone"`
}
type CourierResponse struct{
    Couriers []Courier `json:"couriers"`
    Count int `json:"count"`
}
