package models

type SalePoint struct {
	ID       string `json:"id"`
	BranchID string `json:"branch_id"`
	Name     string `json:"name"`
	IncomeID string `json:"income_id"`
}


type SalePointResponse struct {
    SalePoints []SalePoint `json:"sale_points"`
    Count      int         `json:"count"`
}
