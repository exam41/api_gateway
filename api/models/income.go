package models

type Income struct {
	ID        string `json:"id"`
	CourierID string `json:"courier_id"`
	Status    string `json:"status"`
}
type IncomeID struct {
	ID string `json:"id"`
}

type IncomeResponse struct {
	Incomes []Income `json:"incomes"`
	Count   int      `json:"count"`
}
