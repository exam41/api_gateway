package models

type IncomeProduct struct {
	ID          string `json:"id"`
	IncomeID    string `json:"income_id"`
	CategoryID  string `json:"category_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	Barcode     string `json:"barcode"`
	Quantity    string `json:"quantity"`
	IncomePrice int    `json:"income_price"`
}

type IncomeProductResponse struct {
	IncomeProducts []IncomeProduct `json:"income_products"`
	Count          int             `json:"count"`
}

type CreateIncomeProduct struct {
	IncomeID    string `json:"income_id"`
	CategoryID  string `json:"category_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	Quantity    int    `json:"quantity"`
	IncomePrice int    `json:"income_price"`
}
