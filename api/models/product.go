package models

type Product struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	Price      int    `json:"price"`
	CategoryID string `json:"category_id"`
	Barcode    string `json:"barcode"`
}

type ProductResponse struct {
    Products []Product `json:"products"`
    Count    int       `json:"count"`
}

