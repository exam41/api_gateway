package models

type Sale struct {
	ID          string `json:"id"`
	Barcode     string `json:"barcode"`
	ShiftID     string `json:"shift_id"`
	SalePointID string `json:"sale_point_id"`
	StaffID     string `json:"staff_id"`
	Status      string `json:"status"`
}

type CreateSale struct{
	ShiftID     string `json:"shift_id"`
	SalePointID string `json:"sale_point_id"`
	StaffID     string `json:"staff_id"`
	Status      string `json:"status"`
}
type SaleResponse struct {
    Sales []Sale `json:"sales"`
    Count int    `json:"count"`
}
