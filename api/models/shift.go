package models

import "time"

type Shift struct {
	ID          string    `json:"id"`
	StaffID     string    `json:"staff_id"`
	SalePointID string    `json:"sale_point_id"`
	StartHour   time.Time `json:"start_hour"`
	EndHour     time.Time `json:"end_hour"`
	Status      string    `json:"status"`
}

type ShiftResponse struct {
	Shifts []Shift `json:"shifts"`
	Count  int     `json:"count"`
}

type Status struct {
	Status string `json:"status"`
}

type ShiftStatus struct {
	ShiftID string `json:"shift_id"`
	Status      string `json:"status"`
}
