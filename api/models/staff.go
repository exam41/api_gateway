package models

type Staff struct{
	ID   string `json:"id"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	Phone string `json:"phone"`
	Login string `json:"login"`
	Password string `json:"password"`
	SalesPointId string `json:"sales_point_id"`
	UserRole string `json:"user_role"`
}
type StaffResponse struct{
    Staffs []Staff `json:"staffs"`
    Count int `json:"count"`
}