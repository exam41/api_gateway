package models

type Storage struct {
	ID          string `json:"id"`
	SalePointID string `json:"sale_point_id"`
	ProductID   string `json:"product_id"`
	Barcode     string `json:"barcode"`
	IncomePrice int    `json:"income_price"`
	Quantity    int    `json:"quantity"`
	TotalPrice  int    `json:"total_price"`
}


type StorageResponse struct {
    Storages []Storage `json:"storages"`
    Count int `json:"count"`
}
