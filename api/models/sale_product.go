package models

type SaleProduct struct {
	ID                   string `json:"id"`
	SaleID               string `json:"sale_id"`
	ProductID            string `json:"product_id"`
	Barcode              string `json:"barcode"`
	Quantity             string `json:"quantity"`
	ExistProductQuantity string `json:"exist_product_quantity"`
	Price                string `json:"price"`
	TotalPrice           string `json:"total_price"`
	Payment              string `json:"payment"`
}

type SaleProductResponse struct {
    SaleProducts []SaleProduct `json:"sale_products"`
    Count        int           `json:"count"`
}

