package handler

import (
	pbr "api_gateway/genproto/repo_service"
	"context"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateIncomeProduct godoc
// @Router       /v1/incomeproduct [POST]
// @Summary      Create a new incomeproduct
// @Description  Create a new incomeproduct
// @Tags         incomeproduct
// @Accept       json
// @Produce      json
// @Param        incomeproduct body models.CreateIncomeProduct true "incomeproduct"
// @Success      201  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateIncomeProduct(c *gin.Context) {

	resp := pbr.IncomeProduct{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	fmt.Println("ss",resp.Quantity)
	incomeproduct, err := h.services.IncomeProduct().Create(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, incomeproduct.Id)

}

// GetIncomeProduct   godoc
// @Router       /v1/incomeproduct/{id} [GET]
// @Summary      Get incomeproduct by id
// @Description  get incomeproduct by id
// @Tags         incomeproduct
// @Accept       json
// @Produce      json
// @Param        id path string true "incomeproduct_id"
// @Success      201  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncomeProduct(c *gin.Context) {

	id := c.Param("id")
	incomeproduct, err := h.services.IncomeProduct().Get(context.Background(), &pbr.PrimaryKeyIncomeProduct{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, incomeproduct)
}

// GetListIncomeProduct godoc
// @Router        /v1/incomeproduct [GET]
// @Summary      Get incomeproduct list
// @Description  get incomeproduct list
// @Tags         incomeproduct
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.IncomeProductResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListIncomeProduct(c *gin.Context) {

	var (
		page, limit int
		err         error
	)

	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	incomeproduct, err := h.services.IncomeProduct().GetList(context.Background(), &pbr.IncomeProductRequest{Search: search, Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, incomeproduct)
}

// UpdateIncomeProduct godoc
// @Router        /v1/incomeproduct/{id} [PUT]
// @Summary      Update incomeproduct
// @Description  Update incomeproduct
// @Tags         incomeproduct
// @Accept       json
// @Produce      json
// @Param 		 id path string true "incomeproduct_id"
// @Param        incomeproduct body models.IncomeProduct true "incomeproduct_body"
// @Success      200  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateIncomeProduct(c *gin.Context) {

	id := c.Param("id")

	resp := pbr.IncomeProduct{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	incomeproduct, err := h.services.IncomeProduct().Update(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, incomeproduct)
}

// DeleteIncomeProduct godoc
// @Router       /v1/incomeproduct/{id} [DELETE]
// @Summary      Delete incomeproduct
// @Description  delete incomeproduct
// @Tags         incomeproduct
// @Accept       json
// @Produce      json
// @Param 		 id path string true "incomeproduct_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteIncomeProduct(c *gin.Context) {

	id := c.Param("id")

	_, err := h.services.IncomeProduct().Delete(context.Background(), &pbr.PrimaryKeyIncomeProduct{Id: id})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, "deleted")
}
