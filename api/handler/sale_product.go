package handler

import (
	pbp "api_gateway/genproto/payment_service"
	"strconv"

	"context"

	"github.com/gin-gonic/gin"
)

// CreateSaleProduct godoc
// @Router       /v1/saleproduct [POST]
// @Summary      Create a new saleproduct
// @Description  Create a new saleproduct
// @Tags         saleproduct
// @Accept       json
// @Produce      json
// @Param        saleproduct body models.SaleProduct true "saleproduct"
// @Success      201  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSaleProduct(c *gin.Context) {

	resp := pbp.CreateSaleProduct{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	product, err := h.services.SaleProduct().Create(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, product)

}

// GetSaleProduct   godoc
// @Router       /v1/saleproduct/{id} [GET]
// @Summary      Get saleproduct by id
// @Description  get saleproduct by id
// @Tags         saleproduct
// @Accept       json
// @Produce      json
// @Param        id path string true "saleproduct_id"
// @Success      201  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSaleProduct(c *gin.Context) {

	id := c.Param("id")
	resp, err := h.services.SaleProduct().Get(context.Background(), &pbp.PrimaryKeySaleProduct{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, resp)
}

// GetListSaleProduct godoc
// @Router        /v1/saleproduct [GET]
// @Summary      Get saleproduct list
// @Description  get saleproduct list
// @Tags         saleproduct
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.SaleProductResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListSaleProduct(c *gin.Context) {

	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	resp, err := h.services.SaleProduct().GetList(context.Background(), &pbp.SaleProductRequest{Page: int32(page), Limit: int32(limit), Search: search})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, resp)
}

// UpdateSaleProduct godoc
// @Router        /v1/saleproduct/{id} [PUT]
// @Summary      Update saleproduct
// @Description  Update saleproduct
// @Tags         saleproduct
// @Accept       json
// @Produce      json
// @Param 		 id path string true "saleproduct_id"
// @Param        saleproduct body models.SaleProduct true "saleproduct_body"
// @Success      200  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSaleProduct(c *gin.Context) {

	id := c.Param("id")
	resp := pbp.SaleProduct{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	product, err := h.services.SaleProduct().Update(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, product)

}

// DeleteSaleProduct godoc
// @Router       /v1/saleproduct/{id} [DELETE]
// @Summary      Delete saleproduct
// @Description  delete saleproduct
// @Tags         saleproduct
// @Accept       json
// @Produce      json
// @Param 		 id path string true "saleproduct_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSaleProduct(c *gin.Context) {

	id := c.Param("id")

	_, err := h.services.SaleProduct().Delete(context.Background(), &pbp.PrimaryKeySaleProduct{Id: id})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, "deleted")
}
