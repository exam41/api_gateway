package handler

import (
	pbr "api_gateway/genproto/repo_service"
	"strconv"

	"context"

	"github.com/gin-gonic/gin"
)

// CreateStorage godoc
// @Router       /v1/storage [POST]
// @Summary      Create a new storage
// @Description  Create a new storage
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        storage body models.Storage true "storage"
// @Success      201  {object}  models.Storage
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateStorage(c *gin.Context) {

	resp := pbr.CreateStorage{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	storage, err := h.services.Storage().Create(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, storage)

}

// GetStorage   godoc
// @Router       /v1/storage/{id} [GET]
// @Summary      Get storage by id
// @Description  get storage by id
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        id path string true "storage_id"
// @Success      201  {object}  models.Storage
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStorage(c *gin.Context) {

	id := c.Param("id")
	storage, err := h.services.Storage().Get(context.Background(), &pbr.PrimaryKeyStorage{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, storage)

}

// GetListStorage godoc
// @Router        /v1/storage [GET]
// @Summary      Get storage list
// @Description  get storage list
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.StorageResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListStorage(c *gin.Context) {

	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	storages, err := h.services.Storage().GetList(context.Background(), &pbr.StorageRequest{Page: int32(page), Limit: int32(limit), Search: search})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, storages)
}


// UpdateStorage godoc
// @Router        /v1/storage/{id} [PUT]
// @Summary      Update storage
// @Description  Update storage
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param        storage body models.Storage true "storage_body"
// @Success      200  {object}  models.Storage
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateStorage(c *gin.Context)  {
	
	id := c.Param("id")
	resp := pbr.Storage{}

	err := c.BindJSON(&resp)
	if err != nil {
        handleResponse(c, h.log, "bad request ", 400, err)
        return
    }
	resp.Id = id
	storage, err := h.services.Storage().Update(context.Background(), &resp)
	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }

	handleResponse(c, h.log, "succes", 200, storage)

}

// DeleteStorage godoc
// @Router       /v1/storage/{id} [DELETE]
// @Summary      Delete storage
// @Description  delete storage
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param 		 id path string true "storage_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteStorage(c *gin.Context) {

	id := c.Param("id")
	_, err := h.services.Storage().Delete(context.Background(), &pbr.PrimaryKeyStorage{Id: id})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, "deleted")
}
