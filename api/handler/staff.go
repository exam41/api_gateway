package handler

import (
	pbp "api_gateway/genproto/payment_service"
	"strconv"

	"context"

	"github.com/gin-gonic/gin"
)


// CreateStaff godoc
// @Router       /v1/staff [POST]
// @Summary      Create a new staff
// @Description  Create a new staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        staff body models.Staff true "staff"
// @Success      201  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateStaff(c *gin.Context)  {
	resp := pbp.CreateStaff{}
    err := c.BindJSON(&resp)
    if err != nil {
        handleResponse(c, h.log, "bad request ", 400, err)
        return
    }
    staff, err := h.services.Staff().Create(context.Background(), &resp)

    if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
    handleResponse(c, h.log, "succes", 200, staff.Id)
	
}



// GetStaff   godoc
// @Router       /v1/staff/{id} [GET]
// @Summary      Get staff by id
// @Description  get staff by id
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        id path string true "staff_id"
// @Success      201  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStaff(c *gin.Context)  {
	
	id := c.Param("id")
	staff, err := h.services.Staff().Get(context.Background(), &pbp.PrimaryKeyStaffs{Id: id})

	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
	handleResponse(c, h.log, "succes", 200, staff)
}



// GetListStaff godoc
// @Router        /v1/staff [GET]
// @Summary      Get staff list
// @Description  get staff list
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.StaffResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListStaff(c *gin.Context)  {
	
	var(
		page, limit int
        err         error
	)

	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
        handleResponse(c, h.log, "error while converting pagestr", 400, err)
        return
    }
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
        handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
        return
    }
	search := c.Query("search")
	resp, err := h.services.Staff().GetList(context.Background(), &pbp.StaffRequest{Page: int32(page), Limit: int32(limit), Search: search})

	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
	handleResponse(c, h.log, "succes", 200, resp)
}



// UpdateStaff godoc
// @Router        /v1/staff/{id} [PUT]
// @Summary      Update staff
// @Description  Update staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param 		 id path string true "Staff_id"
// @Param        staff body models.Staff true "Staff_body"
// @Success      200  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateStaff(c *gin.Context)  {
	
	id := c.Param("id")
	resp := pbp.Staffs{}
	err := c.BindJSON(&resp)
	if err != nil {
        handleResponse(c, h.log, "bad request ", 400, err)
        return
    }
	resp.Id = id
	staff, err := h.services.Staff().Update(context.Background(), &resp)
	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
	handleResponse(c, h.log, "succes", 200, staff)
}


// DeleteStaff godoc
// @Router       /v1/staff/{id} [DELETE]
// @Summary      Delete staff
// @Description  delete staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param 		 id path string true "staff_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteStaff(c *gin.Context)  {
	
	id := c.Param("id")
	_, err := h.services.Staff().Delete(context.Background(), &pbp.PrimaryKeyStaffs{Id: id})
	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
	handleResponse(c, h.log, "succes", 200, "deleted")
}