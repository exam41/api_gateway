package handler

import (
	"context"
	"fmt"

	pbp "api_gateway/genproto/payment_service"
	pbr "api_gateway/genproto/repo_service"

	"github.com/gin-gonic/gin"
)

// Incomelogic        godoc
// @Router       /v1/incomelogic/ [POST]
// @Summary      income logic
// @Description  income logic
// @Tags         incomelogic
// @Accept       json
// @Produce      json
// @Param        id body models.IncomeID true "income_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) Incomelogic(c *gin.Context) {
	reps := pbr.Income{}
	if err := c.ShouldBindJSON(&reps); err != nil {
		handleResponse(c, h.log, "error", 500, err)
	}
	fmt.Println("reps",reps.Id)
	updatedincome, err := h.services.Income().EndIncome(context.Background(), &pbr.PrimaryKeyIncome{Id: reps.Id})
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	fmt.Println("param", reps.Id)
	fmt.Println("updatedincome", updatedincome.Id)
	handleResponse(c, h.log, "updated", 200, "here it is")
}

// EndSale        godoc
// @Router        /v1/endsale/ [POST]
// @Summary      ensale
// @Description  endsale
// @Tags         salelogic
// @Accept       json
// @Produce      json
// @Param        sale_id path string true "sale_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) EndSale(c *gin.Context) {
	sale_id := c.Param("sale_id")

	_, err := h.services.Sale().EndSale(context.Background(), &pbp.PrimaryKeySale{Id: sale_id})
	if err != nil {
		handleResponse(c, h.log, "some error occured ", 400, err)
		return
	}

	handleResponse(c, h.log, "updated", 201, "success")

}

// ChangeShiftStatus godoc
// @Router        /v1/shiftlogic/ [PATCH]
// @Summary      Change Shift Status
// @Description  Change Shift Status
// @Tags         changeShiftStatus
// @Accept       json
// @Produce      json
// @Param        shift_status body models.ShiftStatus  true "shift_status"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) ChangeShiftStatus(c *gin.Context) {

	resp := pbp.UpdateStatus{}

	if err := c.ShouldBind(&resp); err != nil {
		handleResponse(c, h.log, " err", 400, err)
		return
	}
	fmt.Println("request", resp.ShiftId, "status", resp.Status)
	status, err := h.services.Shift().Patch(context.Background(), &pbp.UpdateStatus{ShiftId: resp.ShiftId, Status: resp.Status})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, status.ShiftId)

}
