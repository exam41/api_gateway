package handler

import (
	pbp "api_gateway/genproto/payment_service"
	"strconv"

	"context"

	"github.com/gin-gonic/gin"
)

// CreateBranch godoc
// @Router       /v1/branch [POST]
// @Summary      Create a new branch
// @Description  Create a new branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        branch body models.Branch true "branch"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateBranch(c *gin.Context) {

	resp := pbp.Branch{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}

	branch, err := h.services.Branch().Create(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "err", 500, err)
		return
	}
	handleResponse(c, h.log, "success", 200, branch)
}

// GetBranch   godoc
// @Router       /v1/branch/{id} [GET]
// @Summary      Get branch by id
// @Description  get branch by id
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_id"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranch(c *gin.Context) {

	id := c.Param("id")
	branch, err := h.services.Branch().Get(context.Background(), &pbp.PrimaryKeyBranch{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error getting branch in handler", 500, err)
		return
	}
	handleResponse(c, h.log, "success getting", 200, branch)
}

// GetListBranch godoc
// @Router        /v1/branch [GET]
// @Summary      Get branch list
// @Description  get branch list
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.BranchResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListBranch(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return

	}
	search := c.Query("search")

	branch, err := h.services.Branch().GetList(context.Background(), &pbp.BranchRequest{Search: search, Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 200, branch)
}

// UpdateBranch godoc
// @Router        /v1/branch/{id} [PUT]
// @Summary      Update branch
// @Description  Update branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param        branch body models.Branch true "branch_body"
// @Success      200  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateBranch(c *gin.Context) {

	id := c.Param("id")

	resp := pbp.Branch{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	branch, err := h.services.Branch().Update(context.Background(), &resp)

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, branch.Id)
}

// DeleteBranch godoc
// @Router       /v1/branch/{id} [DELETE]
// @Summary      Delete branch
// @Description  delete branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteBranch(c *gin.Context) {
	id := c.Param("id")
	_, err := h.services.Branch().Delete(context.Background(), &pbp.PrimaryKeyBranch{Id: id})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, "deleted")
}
