package handler

import (
	"context"

	pbp "api_gateway/genproto/repo_service"

	"github.com/gin-gonic/gin"
)

// CreateCategory godoc
// @Router       /v1/category [POST]
// @Summary      Create a new category
// @Description  Create a new category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        category body models.CreateCategory true "category"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCategory(c *gin.Context) {
	resp := pbp.CreateCategory{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	category, err := h.services.Category().Create(context.Background(), &resp)

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, category)

}

// GetCategory   godoc
// @Router       /v1/category/{id} [GET]
// @Summary      Get category by id
// @Description  get category by id
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category_id"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategory(c *gin.Context) {

	id := c.Param("id")
	category, err := h.services.Category().Get(context.Background(), &pbp.PrimaryKeyCategory{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, category)
}

// GetListCategory godoc
// @Router        /v1/category [GET]
// @Summary      Get category list
// @Description  get category list
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.CategoryResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListCategory(c *gin.Context) {
	var (
		page, limit int
		err         error
	)
	search := c.Query("search")
	category, err := h.services.Category().GetList(context.Background(), &pbp.CategoryRequest{Search: search, Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, category)

}

// UpdateCategory godoc
// @Router        /v1/category/{id} [PUT]
// @Summary      Update category
// @Description  Update category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param 		 id path string true "category_id"
// @Param        category body models.Category true "category_body"
// @Success      200  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCategory(c *gin.Context) {

	id := c.Param("id")
	resp := pbp.Category{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	category, err := h.services.Category().Update(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, category)

}




// DeleteCategory godoc
// @Router       /v1/category/{id} [DELETE]
// @Summary      Delete category
// @Description  delete category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param 		 id path string true "category_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCategory(c *gin.Context)  {

	id := c.Param("id")
	_, err := h.services.Category().Delete(context.Background(), &pbp.PrimaryKeyCategory{Id: id})
	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
	handleResponse(c, h.log, "succes", 200, "deleted")
}
