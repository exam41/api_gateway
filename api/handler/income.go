package handler

import (
	pbr "api_gateway/genproto/repo_service"
	"context"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateIncome godoc
// @Router       /v1/income [POST]
// @Summary      Create a new income
// @Description  Create a new income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        income body models.Income true "income"
// @Success      201  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateIncome(c *gin.Context) {

	resp := pbr.Income{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	income, err := h.services.Income().Create(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}

	handleResponse(c, h.log, "succes", 201, income)

}

// GetIncome   godoc
// @Router       /v1/income/{id} [GET]
// @Summary      Get income by id
// @Description  get income by id
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        id path string true "income_id"
// @Success      201  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncome(c *gin.Context) {

	id := c.Param("id")

	income, err := h.services.Income().Get(context.Background(), &pbr.PrimaryKeyIncome{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, income)
}

// GetListIncome godoc
// @Router        /v1/income [GET]
// @Summary      Get income list
// @Description  get income list
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.IncomeResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListIncome(c *gin.Context) {

	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	income, err := h.services.Income().GetList(context.Background(), &pbr.IncomeRequest{Search: search, Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, income)

}

// UpdateIncome godoc
// @Router        /v1/income/{id} [PUT]
// @Summary      Update income
// @Description  Update income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Param        income body models.Income true "income_body"
// @Success      200  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateIncome(c *gin.Context) {

	id := c.Param("id")
	resp := pbr.Income{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	income, err := h.services.Income().Update(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, income)
}

// DeleteIncome godoc
// @Router       /v1/income/{id} [DELETE]
// @Summary      Delete income
// @Description  delete income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteIncome(c *gin.Context) {
	id := c.Param("id")
	_, err := h.services.Income().Delete(context.Background(), &pbr.PrimaryKeyIncome{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, "deleted")

}
