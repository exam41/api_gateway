package handler

import (
	pbp "api_gateway/genproto/payment_service"
	"fmt"
	"strconv"

	"context"

	"github.com/gin-gonic/gin"
)

// CreateSalePoint godoc
// @Router       /v1/salepoint [POST]
// @Summary      Create a new salepoint
// @Description  Create a new salepoint
// @Tags         salepoint
// @Accept       json
// @Produce      json
// @Param        salepoint body models.SalePoint true "salepoint"
// @Success      201  {object}  models.SalePoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSalePoint(c *gin.Context) {

	resp := pbp.SalesPoint{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	sale, err := h.services.SalePoint().Create(context.Background(), &resp)

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		fmt.Println("errrr",sale.Id)
		return
	}
	handleResponse(c, h.log, "succes", 201, sale)

}

// GetSalePoint   godoc
// @Router       /v1/salepoint/{id} [GET]
// @Summary      Get salepoint by id
// @Description  get salepoint by id
// @Tags         salepoint
// @Accept       json
// @Produce      json
// @Param        id path string true "salepoint_id"
// @Success      201  {object}  models.SalePoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSalePoint(c *gin.Context) {

	id := c.Param("id")

	sale, err := h.services.SalePoint().Get(context.Background(), &pbp.PrimaryKeySalePoint{Id: id})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, sale)
}

// GetListSalePoint godoc
// @Router        /v1/salepoint [GET]
// @Summary      Get salepoint list
// @Description  get salepoint list
// @Tags         salepoint
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.SalePointResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListSalePoint(c *gin.Context) {

	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	resp, err := h.services.SalePoint().GetList(context.Background(), &pbp.SalePointRequest{Page: int32(page), Limit: int32(limit), Search: search})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, resp)
}




// UpdateSalePoint godoc
// @Router        /v1/salepoint/{id} [PUT]
// @Summary      Update salepoint
// @Description  Update salepoint
// @Tags         salepoint
// @Accept       json
// @Produce      json
// @Param 		 id path string true "SalePoint_id"
// @Param        salepoint body models.SalePoint true "salepoint_body"
// @Success      200  {object}  models.SalePoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSalePoint(c *gin.Context)  {
	
	id := c.Param("id")
	resp := pbp.SalesPoint{}
	err := c.BindJSON(&resp)
	if err != nil {
        handleResponse(c, h.log, "bad request ", 400, err)
        return
    }
	resp.Id = id
	sale, err := h.services.SalePoint().Update(context.Background(), &resp)
	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
	handleResponse(c, h.log, "succes", 200, sale)
}


// DeleteSalePoint godoc
// @Router       /v1/salepoint/{id} [DELETE]
// @Summary      Delete salepoint
// @Description  delete salepoint
// @Tags         salepoint
// @Accept       json
// @Produce      json
// @Param 		 id path string true "salepoint_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSalePoint(c *gin.Context)  {
	
	id := c.Param("id")
	_,err := h.services.SalePoint().Delete(context.Background(), &pbp.PrimaryKeySalePoint{Id: id})
	if err != nil {
        handleResponse(c, h.log, "error ", 500, err)
        return
    }
	handleResponse(c, h.log, "succes", 200, "deleted")
}