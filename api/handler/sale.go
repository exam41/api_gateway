package handler

import (
	pbp "api_gateway/genproto/payment_service"
	"strconv"

	"context"

	"github.com/gin-gonic/gin"
)

// CreateSale godoc
// @Router       /v1/sale [POST]
// @Summary      Create a new sale
// @Description  Create a new sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        sale body models.CreateSale true "sale"
// @Success      201  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSale(c *gin.Context) {

	resp := pbp.Sales{}

	// shift, err := h.services.Shift().Get(context.Background(), &pbp.PrimaryKeyShift{Id: resp.ShiftId})
	// if err != nil {
	// 	handleResponse(c, h.log, "error ", 500, err)
	// 	return
	// }
	// fmt.Println("ff",shift.Status)
	// if shift.Status == "opened" {
	// 	handleResponse(c, h.log, "close shift first", 200, "")

	// } else {
	if err := c.BindJSON(&resp); err != nil {
		handleResponse(c, h.log, "err", 500, err)
		return
	}
	sale, err := h.services.Sale().Create(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, sale.Id)

}

// GetSale   godoc
// @Router       /v1/sale/{id} [GET]
// @Summary      Get sale by id
// @Description  get sale by id
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_id"
// @Success      201  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSale(c *gin.Context) {

	id := c.Param("id")
	sale, err := h.services.Sale().Get(context.Background(), &pbp.PrimaryKeySale{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, sale)
}

// GetListSale godoc
// @Router        /v1/sale [GET]
// @Summary      Get sale list
// @Description  get sale list
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.SaleResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListSale(c *gin.Context) {

	pagestring := c.DefaultQuery("page", "1")
	page, err := strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err := strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	resp, err := h.services.Sale().GetList(context.Background(), &pbp.SaleRequest{Page: int32(page), Limit: int32(limit), Search: search})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, resp)
}

// UpdateSale godoc
// @Router        /v1/sale/{id} [PUT]
// @Summary      Update sale
// @Description  Update sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param 		 id path string true "sale_id"
// @Param        sale body models.Sale true "sale_id"
// @Success      200  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSale(c *gin.Context) {

	id := c.Param("id")
	resp := pbp.Sales{}

	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	sale, err := h.services.Sale().Update(context.Background(), &resp)

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, sale)

}

// DeleteSale godoc
// @Router       /v1/sale/{id} [DELETE]
// @Summary      Delete sale
// @Description  delete sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param 		 id path string true "sale_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSale(c *gin.Context) {

	id := c.Param("id")
	_, err := h.services.Sale().Delete(context.Background(), &pbp.PrimaryKeySale{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, nil)

}
