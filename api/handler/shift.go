package handler

import (
	pbp "api_gateway/genproto/payment_service"
	"strconv"

	"context"

	"github.com/gin-gonic/gin"
)

// CreateShift godoc
// @Router       /v1/shift [POST]
// @Summary      Create a new shift
// @Description  Create a new shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        shift body models.Shift true "shift"
// @Success      201  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateShift(c *gin.Context) {

	resp := pbp.Shifts{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	shift, err := h.services.Shift().Create(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, shift)

}

// GetShift   godoc
// @Router       /v1/shift/{id} [GET]
// @Summary      Get shift by id
// @Description  get shift by id
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        id path string true "shift_id"
// @Success      201  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetShift(c *gin.Context) {

	id := c.Param("id")

	shift, err := h.services.Shift().Get(context.Background(), &pbp.PrimaryKeyShift{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, shift)

}

// GetListShift godoc
// @Router        /v1/shift [GET]
// @Summary      Get shift list
// @Description  get shift list
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.ShiftResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListShift(c *gin.Context) {

	var (
		page, limit int
		err         error
	)

	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	resp, err := h.services.Shift().GetList(context.Background(), &pbp.ShiftRequest{Page: int32(page), Limit: int32(limit), Search: search})

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, resp)
}

// UpdateShift godoc
// @Router        /v1/shift/{id} [PUT]
// @Summary      Update shift
// @Description  Update shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param 		 id path string true "Shift_id"
// @Param        shift body models.Shift true "Shift_body"
// @Success      200  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateShift(c *gin.Context) {

	id := c.Param("id")
	resp := pbp.Shifts{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	shift, err := h.services.Shift().Update(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, shift)
}

// DeleteShift godoc
// @Router       /v1/shift/{id} [DELETE]
// @Summary      Delete shift
// @Description  delete shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param 		 id path string true "shift_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteShift(c *gin.Context) {

	id := c.Param("id")
	_, err := h.services.Shift().Delete(context.Background(), &pbp.PrimaryKeyShift{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, "deleted")
}

// UpdateStatusShift godoc
//@Router            /v1/shift/{id} [PATCH]
//@Summary           Update status shift
//@Description       update status shift
//@Tags              shift
//@Accept            json
//@Produce           json
//@Param             id path string true "shift_id"
//@Param             status body models.Status true "shift_status"
// @Success          200  {object}  models.Response
// @Failure          400  {object}  models.Response
// @Failure          404  {object}  models.Response
// @Failure          500  {object}  models.Response
func (h Handler) UpdateStatusShift(c *gin.Context) {

}
