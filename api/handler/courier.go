package handler

import (
	pbp "api_gateway/genproto/payment_service"
	"context"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateCourier godoc
// @Router       /v1/courier [POST]
// @Summary      Create a new courier
// @Description  Create a new courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        courier body models.CreateCourier true "courier"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCourier(c *gin.Context) {

	resp := pbp.CreateCourier{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	courier, err := h.services.Courier().Create(context.Background(), &resp)

	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 201, courier.Id)

}

// GetCourier   godoc
// @Router       /v1/courier/{id} [GET]
// @Summary      Get courier by id
// @Description  get courier by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourier(c *gin.Context) {

	id := c.Param("id")
	courier, err := h.services.Courier().Get(context.Background(), &pbp.PrimaryKeyCourier{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error getting courier in handler", 500, err)
		return
	}
	handleResponse(c, h.log, "success getting", 200, courier)
}

// GetListCourier godoc
// @Router        /v1/courier [GET]
// @Summary      Get courier list
// @Description  get courier list
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.CourierResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetListCourier(c *gin.Context) {

	var (
		page, limit int
		err         error
	)
	pagestring := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pagestring)
	if err != nil {
		handleResponse(c, h.log, "error while converting pagestr", 400, err)
		return
	}
	limitstr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitstr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit ", 400, err.Error())
		return
	}
	search := c.Query("search")
	courier, err := h.services.Courier().GetList(context.Background(), &pbp.CourierRequest{Search: search, Limit: int32(limit), Page: int32(page)})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, courier)

}

// UpdateCourier godoc
// @Router        /v1/courier/{id} [PUT]
// @Summary      Update courier
// @Description  Update courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param 		 id path string true "courier_id"
// @Param        courier body models.Courier true "courier_body"
// @Success      200  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCourier(c *gin.Context) {
	id := c.Param("id")

	resp := pbp.Courier{}
	err := c.BindJSON(&resp)
	if err != nil {
		handleResponse(c, h.log, "bad request ", 400, err)
		return
	}
	resp.Id = id
	courier, err := h.services.Courier().Update(context.Background(), &resp)
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, courier.Id)

}

// DeleteCourier godoc
// @Router       /v1/courier/{id} [DELETE]
// @Summary      Delete courier
// @Description  delete courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param 		 id path string true "courier_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCourier(c *gin.Context) {

	id := c.Param("id")
	_, err := h.services.Courier().Delete(context.Background(), &pbp.PrimaryKeyCourier{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error ", 500, err)
		return
	}
	handleResponse(c, h.log, "succes", 200, "deleted")

}
