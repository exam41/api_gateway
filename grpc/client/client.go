package client

import (
	"api_gateway/config"
	pbp "api_gateway/genproto/payment_service"
	pbr "api_gateway/genproto/repo_service"

	"google.golang.org/grpc"
)

type IServiceManager interface {

	//pay service
	Branch() pbp.BranchesServiceClient
	Sale() pbp.SalesServiceClient
	SalePoint() pbp.SalesPointsServiceClient
	SaleProduct() pbp.SaleProductsServiceClient
	Courier() pbp.CourierServiceClient
	Shift() pbp.ShiftsServiceClient
	Staff() pbp.StaffsServiceClient

	// repo service
	Category() pbr.CategoryServiceClient
	Product() pbr.ProductServiceClient
	Income() pbr.IncomeServiceClient
	IncomeProduct() pbr.IncomeProductServiceClient
	Storage() pbr.StorageServiceClient
}

type grpcClient struct {

	// pay service
	branchService      pbp.BranchesServiceClient
	courierService     pbp.CourierServiceClient
	saleService        pbp.SalesServiceClient
	shiftService       pbp.ShiftsServiceClient
	staffService       pbp.StaffsServiceClient
	salePointService   pbp.SalesPointsServiceClient
	saleProductService pbp.SaleProductsServiceClient

	//repo service

	categoryService      pbr.CategoryServiceClient
	productService       pbr.ProductServiceClient
	incomeService        pbr.IncomeServiceClient
	incomeProductService pbr.IncomeProductServiceClient
	storageService       pbr.StorageServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connPay, err := grpc.Dial(
		"localhost:8089",
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	connRepo, err := grpc.Dial(
		cfg.RepoGRPCServiceHost+cfg.RepoGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	return &grpcClient{
		branchService:      pbp.NewBranchesServiceClient(connPay),
		courierService:     pbp.NewCourierServiceClient(connPay),
		saleService:        pbp.NewSalesServiceClient(connPay),
		shiftService:       pbp.NewShiftsServiceClient(connPay),
		staffService:       pbp.NewStaffsServiceClient(connPay),
		salePointService:   pbp.NewSalesPointsServiceClient(connPay),
		saleProductService: pbp.NewSaleProductsServiceClient(connPay),

		categoryService:      pbr.NewCategoryServiceClient(connRepo),
		productService:       pbr.NewProductServiceClient(connRepo),
		incomeService:        pbr.NewIncomeServiceClient(connRepo),
		incomeProductService: pbr.NewIncomeProductServiceClient(connRepo),
		storageService:       pbr.NewStorageServiceClient(connRepo),
	}, nil

}

func (g *grpcClient) Branch() pbp.BranchesServiceClient {
    return g.branchService
}

func (g *grpcClient) Sale() pbp.SalesServiceClient {
    return g.saleService
}

func (g *grpcClient) SalePoint() pbp.SalesPointsServiceClient {
    return g.salePointService
}

func (g *grpcClient) SaleProduct() pbp.SaleProductsServiceClient {
    return g.saleProductService
}

func (g *grpcClient) Courier() pbp.CourierServiceClient {
    return g.courierService
}

func (g *grpcClient) Shift() pbp.ShiftsServiceClient {
    return g.shiftService
}

func (g *grpcClient) Staff() pbp.StaffsServiceClient {
    return g.staffService
}

func (g *grpcClient) Category() pbr.CategoryServiceClient {
    return g.categoryService
}

func (g *grpcClient) Product() pbr.ProductServiceClient {
    return g.productService
}

func (g *grpcClient) Income() pbr.IncomeServiceClient {
    return g.incomeService
}

func (g *grpcClient) IncomeProduct() pbr.IncomeProductServiceClient {
    return g.incomeProductService
}

func (g *grpcClient) Storage() pbr.StorageServiceClient {
    return g.storageService
}

