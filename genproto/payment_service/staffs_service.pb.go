// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: staffs_service.proto

package payment_service

import (
	empty "github.com/golang/protobuf/ptypes/empty"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CreateStaff struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	FirstName    string `protobuf:"bytes,1,opt,name=first_name,json=firstName,proto3" json:"first_name,omitempty"`
	LastName     string `protobuf:"bytes,2,opt,name=last_name,json=lastName,proto3" json:"last_name,omitempty"`
	Phone        string `protobuf:"bytes,3,opt,name=phone,proto3" json:"phone,omitempty"`
	Login        string `protobuf:"bytes,4,opt,name=login,proto3" json:"login,omitempty"`
	Password     string `protobuf:"bytes,5,opt,name=password,proto3" json:"password,omitempty"`
	SalesPointId string `protobuf:"bytes,6,opt,name=sales_point_id,json=salesPointId,proto3" json:"sales_point_id,omitempty"`
	UserRole     string `protobuf:"bytes,7,opt,name=user_role,json=userRole,proto3" json:"user_role,omitempty"`
}

func (x *CreateStaff) Reset() {
	*x = CreateStaff{}
	if protoimpl.UnsafeEnabled {
		mi := &file_staffs_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateStaff) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateStaff) ProtoMessage() {}

func (x *CreateStaff) ProtoReflect() protoreflect.Message {
	mi := &file_staffs_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateStaff.ProtoReflect.Descriptor instead.
func (*CreateStaff) Descriptor() ([]byte, []int) {
	return file_staffs_service_proto_rawDescGZIP(), []int{0}
}

func (x *CreateStaff) GetFirstName() string {
	if x != nil {
		return x.FirstName
	}
	return ""
}

func (x *CreateStaff) GetLastName() string {
	if x != nil {
		return x.LastName
	}
	return ""
}

func (x *CreateStaff) GetPhone() string {
	if x != nil {
		return x.Phone
	}
	return ""
}

func (x *CreateStaff) GetLogin() string {
	if x != nil {
		return x.Login
	}
	return ""
}

func (x *CreateStaff) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

func (x *CreateStaff) GetSalesPointId() string {
	if x != nil {
		return x.SalesPointId
	}
	return ""
}

func (x *CreateStaff) GetUserRole() string {
	if x != nil {
		return x.UserRole
	}
	return ""
}

type Staffs struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	FirstName    string `protobuf:"bytes,2,opt,name=first_name,json=firstName,proto3" json:"first_name,omitempty"`
	LastName     string `protobuf:"bytes,3,opt,name=last_name,json=lastName,proto3" json:"last_name,omitempty"`
	Phone        string `protobuf:"bytes,4,opt,name=phone,proto3" json:"phone,omitempty"`
	Login        string `protobuf:"bytes,5,opt,name=login,proto3" json:"login,omitempty"`
	Password     string `protobuf:"bytes,6,opt,name=password,proto3" json:"password,omitempty"`
	SalesPointId string `protobuf:"bytes,7,opt,name=sales_point_id,json=salesPointId,proto3" json:"sales_point_id,omitempty"`
	UserRole     string `protobuf:"bytes,8,opt,name=user_role,json=userRole,proto3" json:"user_role,omitempty"`
}

func (x *Staffs) Reset() {
	*x = Staffs{}
	if protoimpl.UnsafeEnabled {
		mi := &file_staffs_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Staffs) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Staffs) ProtoMessage() {}

func (x *Staffs) ProtoReflect() protoreflect.Message {
	mi := &file_staffs_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Staffs.ProtoReflect.Descriptor instead.
func (*Staffs) Descriptor() ([]byte, []int) {
	return file_staffs_service_proto_rawDescGZIP(), []int{1}
}

func (x *Staffs) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Staffs) GetFirstName() string {
	if x != nil {
		return x.FirstName
	}
	return ""
}

func (x *Staffs) GetLastName() string {
	if x != nil {
		return x.LastName
	}
	return ""
}

func (x *Staffs) GetPhone() string {
	if x != nil {
		return x.Phone
	}
	return ""
}

func (x *Staffs) GetLogin() string {
	if x != nil {
		return x.Login
	}
	return ""
}

func (x *Staffs) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

func (x *Staffs) GetSalesPointId() string {
	if x != nil {
		return x.SalesPointId
	}
	return ""
}

func (x *Staffs) GetUserRole() string {
	if x != nil {
		return x.UserRole
	}
	return ""
}

type PrimaryKeyStaffs struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *PrimaryKeyStaffs) Reset() {
	*x = PrimaryKeyStaffs{}
	if protoimpl.UnsafeEnabled {
		mi := &file_staffs_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PrimaryKeyStaffs) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PrimaryKeyStaffs) ProtoMessage() {}

func (x *PrimaryKeyStaffs) ProtoReflect() protoreflect.Message {
	mi := &file_staffs_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PrimaryKeyStaffs.ProtoReflect.Descriptor instead.
func (*PrimaryKeyStaffs) Descriptor() ([]byte, []int) {
	return file_staffs_service_proto_rawDescGZIP(), []int{2}
}

func (x *PrimaryKeyStaffs) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type StaffRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Limit  int32  `protobuf:"varint,1,opt,name=limit,proto3" json:"limit,omitempty"`
	Page   int32  `protobuf:"varint,2,opt,name=page,proto3" json:"page,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *StaffRequest) Reset() {
	*x = StaffRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_staffs_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StaffRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StaffRequest) ProtoMessage() {}

func (x *StaffRequest) ProtoReflect() protoreflect.Message {
	mi := &file_staffs_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StaffRequest.ProtoReflect.Descriptor instead.
func (*StaffRequest) Descriptor() ([]byte, []int) {
	return file_staffs_service_proto_rawDescGZIP(), []int{3}
}

func (x *StaffRequest) GetLimit() int32 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *StaffRequest) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *StaffRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type StaffResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Staffs []*Staffs `protobuf:"bytes,1,rep,name=staffs,proto3" json:"staffs,omitempty"`
	Count  int32     `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
}

func (x *StaffResponse) Reset() {
	*x = StaffResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_staffs_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StaffResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StaffResponse) ProtoMessage() {}

func (x *StaffResponse) ProtoReflect() protoreflect.Message {
	mi := &file_staffs_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StaffResponse.ProtoReflect.Descriptor instead.
func (*StaffResponse) Descriptor() ([]byte, []int) {
	return file_staffs_service_proto_rawDescGZIP(), []int{4}
}

func (x *StaffResponse) GetStaffs() []*Staffs {
	if x != nil {
		return x.Staffs
	}
	return nil
}

func (x *StaffResponse) GetCount() int32 {
	if x != nil {
		return x.Count
	}
	return 0
}

var File_staffs_service_proto protoreflect.FileDescriptor

var file_staffs_service_proto_rawDesc = []byte{
	0x0a, 0x14, 0x73, 0x74, 0x61, 0x66, 0x66, 0x73, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0f, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0xd4, 0x01, 0x0a, 0x0b, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53,
	0x74, 0x61, 0x66, 0x66, 0x12, 0x1d, 0x0a, 0x0a, 0x66, 0x69, 0x72, 0x73, 0x74, 0x5f, 0x6e, 0x61,
	0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x66, 0x69, 0x72, 0x73, 0x74, 0x4e,
	0x61, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x6c, 0x61, 0x73, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6c, 0x61, 0x73, 0x74, 0x4e, 0x61, 0x6d, 0x65,
	0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x12, 0x1a, 0x0a, 0x08,
	0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x12, 0x24, 0x0a, 0x0e, 0x73, 0x61, 0x6c, 0x65,
	0x73, 0x5f, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0c, 0x73, 0x61, 0x6c, 0x65, 0x73, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x1b,
	0x0a, 0x09, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x72, 0x6f, 0x6c, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x52, 0x6f, 0x6c, 0x65, 0x22, 0xdf, 0x01, 0x0a, 0x06,
	0x53, 0x74, 0x61, 0x66, 0x66, 0x73, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x66, 0x69, 0x72, 0x73, 0x74, 0x5f,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x66, 0x69, 0x72, 0x73,
	0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x6c, 0x61, 0x73, 0x74, 0x5f, 0x6e, 0x61,
	0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6c, 0x61, 0x73, 0x74, 0x4e, 0x61,
	0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x6f, 0x67, 0x69,
	0x6e, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x12, 0x1a,
	0x0a, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x12, 0x24, 0x0a, 0x0e, 0x73, 0x61,
	0x6c, 0x65, 0x73, 0x5f, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0c, 0x73, 0x61, 0x6c, 0x65, 0x73, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x49, 0x64,
	0x12, 0x1b, 0x0a, 0x09, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x72, 0x6f, 0x6c, 0x65, 0x18, 0x08, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x52, 0x6f, 0x6c, 0x65, 0x22, 0x22, 0x0a,
	0x10, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x53, 0x74, 0x61, 0x66, 0x66,
	0x73, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x22, 0x50, 0x0a, 0x0c, 0x53, 0x74, 0x61, 0x66, 0x66, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05,
	0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x70, 0x61, 0x67, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x22, 0x56, 0x0a, 0x0d, 0x53, 0x74, 0x61, 0x66, 0x66, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2f, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x66, 0x66, 0x73, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x74, 0x61, 0x66, 0x66, 0x73, 0x52, 0x06, 0x73,
	0x74, 0x61, 0x66, 0x66, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x32, 0xde, 0x02, 0x0a, 0x0d,
	0x53, 0x74, 0x61, 0x66, 0x66, 0x73, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3f, 0x0a,
	0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x1c, 0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e,
	0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x53, 0x74, 0x61, 0x66, 0x66, 0x1a, 0x17, 0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x74, 0x61, 0x66, 0x66, 0x73, 0x12, 0x41,
	0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x21, 0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b,
	0x65, 0x79, 0x53, 0x74, 0x61, 0x66, 0x66, 0x73, 0x1a, 0x17, 0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d,
	0x6e, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x74, 0x61, 0x66, 0x66,
	0x73, 0x12, 0x48, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x1d, 0x2e, 0x70,
	0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53,
	0x74, 0x61, 0x66, 0x66, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1e, 0x2e, 0x70, 0x61,
	0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x74,
	0x61, 0x66, 0x66, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3a, 0x0a, 0x06, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x17, 0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x74, 0x61, 0x66, 0x66, 0x73, 0x1a, 0x17,
	0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x53, 0x74, 0x61, 0x66, 0x66, 0x73, 0x12, 0x43, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x12, 0x21, 0x2e, 0x70, 0x61, 0x79, 0x65, 0x6d, 0x6e, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x53, 0x74,
	0x61, 0x66, 0x66, 0x73, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x42, 0x1a, 0x5a, 0x18,
	0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_staffs_service_proto_rawDescOnce sync.Once
	file_staffs_service_proto_rawDescData = file_staffs_service_proto_rawDesc
)

func file_staffs_service_proto_rawDescGZIP() []byte {
	file_staffs_service_proto_rawDescOnce.Do(func() {
		file_staffs_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_staffs_service_proto_rawDescData)
	})
	return file_staffs_service_proto_rawDescData
}

var file_staffs_service_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_staffs_service_proto_goTypes = []interface{}{
	(*CreateStaff)(nil),      // 0: payemnt_service.CreateStaff
	(*Staffs)(nil),           // 1: payemnt_service.Staffs
	(*PrimaryKeyStaffs)(nil), // 2: payemnt_service.PrimaryKeyStaffs
	(*StaffRequest)(nil),     // 3: payemnt_service.StaffRequest
	(*StaffResponse)(nil),    // 4: payemnt_service.StaffResponse
	(*empty.Empty)(nil),      // 5: google.protobuf.Empty
}
var file_staffs_service_proto_depIdxs = []int32{
	1, // 0: payemnt_service.StaffResponse.staffs:type_name -> payemnt_service.Staffs
	0, // 1: payemnt_service.StaffsService.Create:input_type -> payemnt_service.CreateStaff
	2, // 2: payemnt_service.StaffsService.Get:input_type -> payemnt_service.PrimaryKeyStaffs
	3, // 3: payemnt_service.StaffsService.GetList:input_type -> payemnt_service.StaffRequest
	1, // 4: payemnt_service.StaffsService.Update:input_type -> payemnt_service.Staffs
	2, // 5: payemnt_service.StaffsService.Delete:input_type -> payemnt_service.PrimaryKeyStaffs
	1, // 6: payemnt_service.StaffsService.Create:output_type -> payemnt_service.Staffs
	1, // 7: payemnt_service.StaffsService.Get:output_type -> payemnt_service.Staffs
	4, // 8: payemnt_service.StaffsService.GetList:output_type -> payemnt_service.StaffResponse
	1, // 9: payemnt_service.StaffsService.Update:output_type -> payemnt_service.Staffs
	5, // 10: payemnt_service.StaffsService.Delete:output_type -> google.protobuf.Empty
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_staffs_service_proto_init() }
func file_staffs_service_proto_init() {
	if File_staffs_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_staffs_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateStaff); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_staffs_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Staffs); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_staffs_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PrimaryKeyStaffs); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_staffs_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StaffRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_staffs_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StaffResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_staffs_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_staffs_service_proto_goTypes,
		DependencyIndexes: file_staffs_service_proto_depIdxs,
		MessageInfos:      file_staffs_service_proto_msgTypes,
	}.Build()
	File_staffs_service_proto = out.File
	file_staffs_service_proto_rawDesc = nil
	file_staffs_service_proto_goTypes = nil
	file_staffs_service_proto_depIdxs = nil
}
